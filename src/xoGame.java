import java.util.InputMismatchException; //กันใส่อักษร
import java.util.Random;
import java.util.Scanner;

public class xoGame {
    static char[][] board = new char[3][3];
    static char turn;

    public static void main(String[] args) {
        System.out.println("Welcome to XO Game");
        board();
        turn = new Random().nextBoolean() ? 'X' : 'O'; // สุ่มผู้เล่นคนแรก
        System.out.println("Turn " + turn);

        while (true) {
            move();
            printBoard();
            if (checkWin()) {
                System.out.println("!!!! " + turn + " WIN !!!!");
                break;
            } else if (boardFull()) {
                System.out.println("It's a draw!");
                break;
            }
            switchPlayer();
        }
    }

    public static void board() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
        printBoard();
    }

    public static void printBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static boolean boardFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-')
                    return false;
            }
        }
        return true;
    }

    public static void switchPlayer() {
        turn = (turn == 'X') ? 'O' : 'X'; // สลับระหว่าง 'X' และ 'O'
        System.out.println("Turn " + turn);

    }

    public static void move() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                System.out.print("please input (row, col): ");
                int row = scanner.nextInt() - 1;
                int col = scanner.nextInt() - 1;

                if (row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-') {
                    board[row][col] = turn;
                    break;
                } else {
                    System.out.println("Try again.");
                }
            } catch (InputMismatchException e) {
                System.out.println("Please enter numbers only.");
                scanner.next(); // เดี๋ยวชีวิตติดลูป
            }
        }
    }

    public static boolean checkWin() {
        for (int i = 0; i < 3; i++) {
            // เช็กนอนตั้ง
            if ((board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != '-')
                    || (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] != '-')) {
                return true;
            }
        }
        // เช็กทะแยง
        if ((board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != '-')
                || (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != '-')) {
            return true;
        }
        return false;
    }

}